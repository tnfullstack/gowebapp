// Lecture 10 - Types & Structs
package main

import (
	"log"
	"time"
)

type User struct {
	FirstName string
	LastName string
	PhoneNum string
	Age int
	BirthDate time.Time
}

func main() {

	chris := User {
		FirstName: "Chris",
		LastName: "Nguyen",
		Age: 49,
		// BirthDate: time.Time,
	}

	log.Println("Chris's Age is", chris.Age)
	log.Println("Chris's birthday is", chris.BirthDate)
}