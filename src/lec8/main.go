// Lecture 8 - Veriables & Functions
package main

import "fmt"

func main() {
	fmt.Println("Hello, world.")

	// Veriable example
	var whatToSay string
	var i int

	whatToSay = "Goodbye, cruel world"
	fmt.Println(whatToSay)

	i = 9
	fmt.Println("i is set to,", i)

	whatWasSaid := saySomething()
	fmt.Println("The function returned", whatWasSaid)

	name, age := sayMorething()
	
	fmt.Println(name, age, "years old!")
}

// Practice function
func saySomething() string {
	return "something"
}

// Function that returns multiple parameters 
func sayMorething() (string, int) {
	return "Chris's age is", 49 
}