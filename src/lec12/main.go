// Lecture 12 - Other data structures: Maps and Slices
package main

import "log"


func main() {

	var myVar string
	var num int

	myVar = "Chris Nguyen"
	num = 49

	log.Println(myVar)
	log.Println(num)
}